const { Router } = require("express");
const controller = require("./controller");
const bcrypt = require("bcryptjs")

const router = Router();

router.post('/register', controller.registerUser);
router.post('/login/', controller.getUsersByEmail);



module.exports = router;