const getUsersByEmail = "SELECT * FROM users WHERE user_email = $1";
const registerUser = "INSERT INTO users (user_id , user_name , user_email , password) VALUES ($1 , $2 , $3, $4)"

module.exports = {
    getUsersByEmail,
    registerUser
}